#include "co_edu_javeriana_wrappers_MyPIDInC.h"
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <thread>
#include <sstream>

int getMyPID(){
    return getpid();
}

JNIEXPORT jint JNICALL Java_co_edu_javeriana_wrappers_MyPIDInC_getMyPID (JNIEnv *, jobject){
     int myPID = getMyPID();
     std::cout<<"Hello From c++";
     //printf("My PID %d -", myPID);
     return myPID;
}

JNIEXPORT jstring JNICALL Java_co_edu_javeriana_wrappers_MyPIDInC_getMyThreadID(JNIEnv *env, jobject thisObject){
     std::thread::id this_id = std::this_thread::get_id();
     std::stringstream ss;
     ss << this_id;
     std::string myThreadPIDAsString = ss.str();
     return env->NewStringUTF(myThreadPIDAsString.c_str());
}

JNIEXPORT jstring JNICALL Java_co_edu_javeriana_wrappers_MyPIDInC_getJsonExample (JNIEnv *env, jobject){
     std::string myJSON = R"({"data":[{"title":"Widget","subtitle":"Esto es la descripción de un widget","children":[{"title":"Paquete1","subtitle":"Esto es la descripción de un paquete1","children":[{"title":"Widget1","subtitle":"Esto es la descripción de un widget1","inputs":["input1","input2"],"outputs":["output"],"parameters":["parameter1","patameter2"]},{"title":"Widget2","subtitle":"Esto es la descripción de un widget2","inputs":["input1","input2"],"outputs":["output"],"parameters":["parameter1","patameter2"]},{"title":"Widget3","subtitle":"Esto es la descripción de un widget3","inputs":["input1","input2"],"outputs":["output"],"parameters":["parameter1","patameter2"]}]}]},{"title":"Functor","subtitle":"Esto es la descripción de un functor","children":[{"title":"Paquete2","subtitle":"Esto es la descripción de un paquete1","children":[{"title":"Functor1","subtitle":"Esto es la descripción de un functor1","inputs":["input1","input2"],"outputs":["output"],"parameters":["parameter1","patameter2"]},{"title":"Functor2","subtitle":"Esto es la descripción de un functor2","inputs":["input1","input2"],"outputs":["output"],"parameters":["parameter1","patameter2"]},{"title":"Functor3","subtitle":"Esto es la descripción de un functor3","inputs":["input1","input2"],"outputs":["output"],"parameters":["parameter1","patameter2"]}]}]},{"title":"Filter","subtitle":"Esto es la descripción de un filter","children":[{"title":"Paquete3","subtitle":"Esto es la descripción de un paquete1","children":[{"title":"Filter1","subtitle":"Esto es la descripción de un filter1","inputs":["input1","input2"],"outputs":["output"],"parameters":["parameter1","patameter2"]},{"title":"Filter2","subtitle":"Esto es la descripción de un filter2","inputs":["input1","input2"],"outputs":["output"],"parameters":["parameter1","patameter2"]},{"title":"Filter3","subtitle":"Esto es la descripción de un filter3","inputs":["input1","input2"],"outputs":["output"],"parameters":["parameter1","patameter2"]}]}]}]})";
     return env->NewStringUTF(myJSON.c_str());
}