package co.edu.javeriana.wrappers;

public class MyPIDInC {
        public native int getMyPID();
        public native String getMyThreadID();
        public native String getJsonExample();
}
