package co.edu.javeriana.controllers;

import co.edu.javeriana.model.TestDTO;
import co.edu.javeriana.wrappers.MyPIDInC;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class TestController {

    MyPIDInC wrapper;

    public TestController() {
        this.wrapper = new MyPIDInC();
    }

    @RequestMapping("/pid")
    public TestDTO getMyPID(){
        TestDTO dto = new TestDTO();
        dto.setProcessID(this.wrapper.getMyPID());
        dto.setThreadCPlusPlus(this.wrapper.getMyThreadID());
        dto.setThreadID(Thread.currentThread().getId());
        return dto;
    }

    @RequestMapping("/json")
        public @ResponseBody JsonNode getJSONExample() throws IOException {
        String jsonString = wrapper.getJsonExample();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode response = mapper.readValue(jsonString,JsonNode.class);
        return response;
    }
}
