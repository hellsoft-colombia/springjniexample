package co.edu.javeriana.model;

public class TestDTO {
    private double ProcessID;
    private double ThreadID;
    private String ThreadCPlusPlus;

    public TestDTO(){

    }

    public TestDTO(double processID, double threadID, String ThreadCPlusPlus) {
        ProcessID = processID;
        ThreadID = threadID;
        ThreadCPlusPlus = ThreadCPlusPlus;
    }

    public double getProcessID() {
        return ProcessID;
    }

    public void setProcessID(double processID) {
        ProcessID = processID;
    }

    public double getThreadID() {
        return ThreadID;
    }

    public void setThreadID(double threadID) {
        ThreadID = threadID;
    }

    public String getThreadCPlusPlus() {
        return ThreadCPlusPlus;
    }

    public void setThreadCPlusPlus(String threadCPlusPlus) {
        ThreadCPlusPlus = threadCPlusPlus;
    }
}
