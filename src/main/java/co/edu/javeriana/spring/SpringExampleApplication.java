package co.edu.javeriana.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"co.edu.javeriana"})
public class SpringExampleApplication {
    static {
        System.loadLibrary("mypidinc");
    }
    public static void main(String[] args) {
        SpringApplication.run(SpringExampleApplication.class, args);
    }

}
