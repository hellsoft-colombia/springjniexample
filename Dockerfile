FROM registry.gitlab.com/hellsoft-colombia/springjniexample/cpplugins-backend:v1.0
ARG JAR_FILE=./target/spring-example-0.0.1-SNAPSHOT.jar
ARG LIBRARY_WRAPPER=./src/main/
ADD ${JAR_FILE} /tmp/cpplugins/app.jar
ADD ${LIBRARY_WRAPPER}/native /tmp/cpplugins/native
RUN mkdir -p /tmp/cpplugins/lib && cd /tmp/cpplugins/native && make
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Djava.library.path=/tmp/cpplugins/lib","-jar","tmp/cpplugins/app.jar"]